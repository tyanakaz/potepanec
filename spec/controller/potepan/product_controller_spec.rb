require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }
  describe 'GET #show' do

    context 'product idのリクエスト' do
      before(:each) do
        get :show, params: { id: product.id }
      end

      it 'レスポンステスト' do
        expect(response.status).to eq 200
      end

      it '@product' do
        expect(assigns(:product)).to eq product
      end

    end
  end
end
