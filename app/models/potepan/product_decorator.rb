Spree::Product.class_eval do
  def related_products
    taxons = Spree::Taxon.includes(:products).references(:products)
               .where(id: self.taxons.ids)
               .where.not('spree_products.id = ?', self.id)

    related_products = taxons.map do |taxon|
      taxon.products
    end

    related_products.flatten.uniq
  end
end
