class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCT_COUNT = 4.freeze

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.sample(RELATED_PRODUCT_COUNT)
  end

end
