class Potepan::CategoriesController < ApplicationController

  def show
     @taxon = Spree::Taxon.find(params[:id])
     @products = category_products(@taxon)
     @taxonomies = Spree::Taxonomy.includes(root: :children)
  end

  def category_products(taxon)
    if taxon.root?
      taxon.leaves.includes(:products).map(&:products).flatten.uniq
    else
      taxon.products
    end
  end

end
